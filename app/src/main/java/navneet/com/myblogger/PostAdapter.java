package navneet.com.myblogger;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import navneet.com.myblogger.models.Category;
import navneet.com.myblogger.models.Post;

/**
 * Created by navneet on 19/03/18.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {
    private ArrayList<Post> records=new ArrayList<>();
    private Context context;
    private ArrayList<Category> category = new ArrayList<>();


    public PostAdapter(ArrayList<Post> records, Context context) {
        this.context=context;
        this.records=records;
    }

    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item, parent, false);
        return new PostAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostAdapter.ViewHolder holder, final int position) {

        if (records.get(position).getCategory()!=null) {
             category = new ArrayList<>(records.get(position).getCategory());
        }

        holder.title.setText(records.get(position).getTitle().get$t());
        holder.desc.setText(Html.fromHtml(records.get(position).getContent().get$t()));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();

                if (category!=null)
                bundle.putParcelableArrayList("category", category);

                Intent intent=new Intent(context,PostDetails.class);
                intent.putExtra("title",records.get(position).getTitle().get$t());
                intent.putExtra("desc",records.get(position).getContent().get$t());
                intent.putExtras(bundle);
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return records.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title,desc;
        private ImageView post_image;
        public ViewHolder(View view) {
            super(view);

            post_image=(ImageView)view.findViewById(R.id.post_image);
            title=(TextView)view.findViewById(R.id.title);
            desc=(TextView)view.findViewById(R.id.desc);
        }
    }
}

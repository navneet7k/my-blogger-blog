package navneet.com.myblogger;

import navneet.com.myblogger.models.BlogResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Navneet Krishna on 24/09/18.
 */
interface RequestInterface {
    @GET("feeds/posts/default?alt=json")
    Call<BlogResponse> getBlogData();
}

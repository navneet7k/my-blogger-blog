package navneet.com.myblogger;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import me.gujun.android.taggroup.TagGroup;
import navneet.com.myblogger.models.Category;

public class PostDetails extends AppCompatActivity {

    private TextView title,desc;
    private ArrayList<Category> category=new ArrayList<>();
    private ArrayList<String> catString=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.post_details);

        TagGroup mTagGroup = (TagGroup) findViewById(R.id.tag_group);

        title=(TextView)findViewById(R.id.title);
        desc=(TextView)findViewById(R.id.desc);
        desc.setMovementMethod(new ScrollingMovementMethod());

        Intent intent=getIntent();
        if (intent!=null) {
            Bundle bdl = intent.getExtras();
            title.setText(intent.getStringExtra("title"));
            desc.setText(Html.fromHtml(intent.getStringExtra("desc")));
            category=bdl.getParcelableArrayList("category");
        }
        for (Category category: category) {
            catString.add(category.getTerm());
        }
        mTagGroup.setTags(catString);
    }
}

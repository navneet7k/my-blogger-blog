package navneet.com.myblogger.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Navneet Krishna on 24/09/18.
 */
public class Post {
    @SerializedName("category")
    @Expose
    private List<Category> category = null;
    @SerializedName("title")
    @Expose
    private Title title;
    @SerializedName("content")
    @Expose
    private Content content;
    @SerializedName("media$thumbnail")
    @Expose
    private Media$thumbnail media$thumbnail;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

    public Media$thumbnail getMedia$thumbnail() {
        return media$thumbnail;
    }

    public void setMedia$thumbnail(Media$thumbnail media$thumbnail) {
        this.media$thumbnail = media$thumbnail;
    }
}

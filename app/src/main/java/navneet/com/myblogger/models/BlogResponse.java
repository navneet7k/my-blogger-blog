package navneet.com.myblogger.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Navneet Krishna on 24/09/18.
 */
public class BlogResponse {
    @SerializedName("feed")
    @Expose
    private BlogFeed blogFeed;

    public BlogFeed getBlogFeed() {
        return blogFeed;
    }

    public void setBlogFeed(BlogFeed blogFeed) {
        this.blogFeed = blogFeed;
    }
}

package navneet.com.myblogger.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Navneet Krishna on 24/09/18.
 */
public class BlogFeed {
    @SerializedName("entry")
    @Expose
    private ArrayList<Post> post = null;

    public ArrayList<Post> getPost() {
        return post;
    }

    public void setPost(ArrayList<Post> post) {
        this.post = post;
    }
}

package navneet.com.myblogger;

import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

import navneet.com.myblogger.models.BlogFeed;
import navneet.com.myblogger.models.BlogResponse;
import navneet.com.myblogger.models.Post;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String MY_PREFS_NAME = "navneet.com.myblogger";
    private RecyclerView recyclerView;
    private PostAdapter postAdapter;
    private ArrayList<Post> posts = new ArrayList<>();
    private ProgressBar progress;
    private String blog_url = "http://www.freshbytelabs.com/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progress = (ProgressBar) findViewById(R.id.progress);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredUrl = prefs.getString("blog", null);
        if (restoredUrl != null) {
            blog_url=restoredUrl;
            progress.setVisibility(View.VISIBLE);
            getBlog();
        } else {
            changeBlog();
        }

//

    }

    private void changeBlog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.details_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);

        final EditText blog_txt = (EditText) dialogView.findViewById(R.id.blog_txt);

        dialogBuilder.setTitle("ADD BLOG");
        dialogBuilder.setMessage("Enter blog url");

        final AlertDialog b = dialogBuilder.create();
        b.setCanceledOnTouchOutside(false);
        Button okButton = (Button) dialogView.findViewById(R.id.ok_button);
        Button cancelButton = (Button) dialogView.findViewById(R.id.cancel_button);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String blog = blog_txt.getText().toString();
                blog_url = blog;
                if (!blog.equals("")) {
                    b.dismiss();
                    progress.setVisibility(View.VISIBLE);
                    getBlog();
                }
                if (blog.equals("")) {
                    blog_txt.setError("Enter an email!");
                }

            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });

        b.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.change_blog:
                changeBlog();
                break;

        }
        return true;
    }

    private void getBlog() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(blog_url)
                .build();
        RequestInterface requestInterface = retrofit.create(RequestInterface.class);
        Call<BlogResponse> call = requestInterface.getBlogData();
        call.enqueue(new Callback<BlogResponse>() {
            @Override
            public void onResponse(Call<BlogResponse> call, Response<BlogResponse> response) {
                progress.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "success!", Toast.LENGTH_SHORT).show();
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        BlogFeed blogFeed = response.body().getBlogFeed();
                        posts = blogFeed.getPost();
                        postAdapter = new PostAdapter(posts, MainActivity.this);
                        recyclerView.setAdapter(postAdapter);
                        SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString("blog", blog_url);
                        editor.apply();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Oops Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BlogResponse> call, Throwable t) {
                progress.setVisibility(View.GONE);
                Toast.makeText(MainActivity.this, "Failure", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
